<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function store(Request $request)
    {
      //echo $request;exit;

      $this->validate($request, array(
        'name' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:customers'],
        'password' => ['required', 'string', 'min:6', 'confirmed'],


    ));
    $user = new Customer();
    $user->name = $request->name;
    $user->email = $request->email;
    $user->password = Hash::make($request->password);


    $user->save();

    return redirect()->route('home');
    }

}
