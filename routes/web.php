<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::post('/createcustomer', 'CustomerController@store')->name('createcustomer');
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth'], function () {
  Route::post('/createadmin', 'AdminController@store')->name('createadmin');
  Route::post('/createmanager', 'ManagerController@store')->name('createmanager');

});
